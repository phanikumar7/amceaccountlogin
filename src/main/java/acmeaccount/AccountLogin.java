package acmeaccount;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class AccountLogin {

	@Parameters({"url","username","password","vendortaxid"})
	@Test()
	public void acmeLogin(String url, String username, String password, String vendortaxid) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(url);
		driver.manage().window().maximize();
		driver.findElementById("email").sendKeys(username);
		driver.findElementById("password").sendKeys(password);
		driver.findElementByXPath("//button[@id='buttonLogin']").click();
		WebElement eleVendor = driver.findElementByXPath("(//div[@class='dropdown'])[5]/button");
		Actions builder = new Actions(driver);
		builder.moveToElement(eleVendor).build().perform();
		driver.findElementByLinkText("Search for Vendor").click();
		driver.findElementById("vendorTaxID").sendKeys(vendortaxid);
		driver.findElementById("buttonSearch").click();
		String vendorName = driver.findElementByXPath("((//table[@class='table']//tr)[2]/td)[1]").getText();
		System.out.println("Display Vendor Name : "+vendorName);
		driver.close();
	}
}
